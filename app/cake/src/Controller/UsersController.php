<?php
namespace App\Controller;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->deny(['view']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($username = null)
    {
        $user = $this->Users->findByUsername($username)->first();
        if ($user) {
            $modifiedUser = $this->Auth->user();
            $modifiedUser['username'] = $user['username'];
            $modifiedUser['email'] = $user['email'];
            $modifiedUser['last_login_at'] = $user['last_login_at'];
            $modifiedUser['active'] = $user['active'];
            $modifiedUser['modified_at'] = $user['modified_at'];
            $modifiedUser['created_at'] = $user['created_at'];
            $modifiedUser['roles'] = (new \App\Controller\TwofactorController())->getUsersRole($user['id']);

            $this->set('user', $modifiedUser);
        } else {
            $this->Flash->error("No such user exists");
            return $this->redirect('/');
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($username = null)
    {
        if ($this->Auth->user('username') === $username) {
            $user = $this->Users->findByUsername($username)->first();
        } else {
            $this->Flash->error(__('Access Denied.'));
            return $this->redirect('/');
        }

        if ($this->request->is(['post'])) {
            $formData = $this->request->getData();

            $sameAsPreviousPassword = (new DefaultPasswordHasher())->check($formData['password'], $user['password']);

            // Password check is kept with only previous password. (Not the list of previously used passwords by that user)
            if ($sameAsPreviousPassword) {
                $this->Flash->error(__("The new password cannot be the same as your previously used passwords."));
            }
            
            $updateduser = $this->Users->patchEntity($user, $formData);

            $errors = $updateduser->errors();

            $badpasswordStatus = (new \App\Controller\RegisterController())->checkBadPasswords($user, $formData['password']);

            $errorStatus = !empty($errors) || $badpasswordStatus;
            
            // Display errors generated after validation of user's data.
            if ($errorStatus) {
                if (!empty($errors)) {
                    foreach ($errors as $error) {
                        foreach ($error as $key => $value) {
                            $this->Flash->error($value);
                        }
                    }
                }
                if ($badpasswordStatus) {
                    $this->Flash->error("Please use a more secure password. Try with a sentence.");
                }
            } else {
                if ($this->Users->save($updateduser)) {
                    $this->Flash->success(__('Your password has been updated.'));
                    return $this->redirect([
                        'action' => 'view',
                        $user->username
                    ]);
                } else {
                    $this->Flash->error(__('The password could not be updated. Please, try again.'));
                }
            }
        }

        $this->set(compact('user', $this->Auth->user()));
    }

    /**
     * Index method
    **/
    public function index()
    {

        // Check if already login
        $username = $this->Auth->user('username');

        // Redirect to view.
        if (!is_null($username)) {
            // Profile (/users/{id})
            return $this->redirect([
                'controller' => 'Users',
                'action' => 'view',
                $username
            ]);
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            // Check if login available
            if ($user) {
                // Check if user deactivated or banned
                if (!$user['active']) {
                    // If last login more than 6 months else banned
                    if (Time::now()->toUnixString() - $user['last_login_at']->toUnixString() > 6*30*24*60*60) {
                        return $this->Flash->error(__('Your account is inactive. Contact an administrator to reactivate it.'));
                    } else {
                        return $this->Flash->error(__('Your account has been banned.'));
                    }
                }

                $session = $this->getRequest()->getSession();
                $modified_user_data['username'] = $user['username'];
                $modified_user_data['2fa_status'] = $user['2fa_status'];
                $modified_user_data['created'] = Time::now();

                // Storing in session variable 'user' : [username, 2fa_key, 2fa_status, created]
                $session->write('user', $modified_user_data);

                // This session_verified = 'false' will be set for every new session. Will be set 'true' once user submit 2FA secret code or setup 2FA for first time.
                $this->getRequest()->getSession()->write('session_verified', 0);

                return $this->redirect([
                    'controller' => 'Twofactor',
                    'action' => 'index',
                ]);
            } else {
                $this->Flash->error(__('Username or password is incorrect'));
            }
        }
    }
}
