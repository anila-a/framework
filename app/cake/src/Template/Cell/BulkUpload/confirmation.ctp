<?php if (isset($entities) and isset($error_list)): ?>
    <?= $this->Html->link('Proceed', [
        'action' => 'add', 'bulk', 
        '?' => ['entities_serialize' => serialize($entities),
        'error_list_serialize' => serialize($error_list),
        'header_serialize' => serialize($header)]],
        ['class' => 'btn btn-primary']) ?>

    <?= $this->Html->link('Cancel', [
        'action' => 'add', 'bulk'],
        ['class' => 'btn btn-primary']) ?>    
<?php endif; ?>