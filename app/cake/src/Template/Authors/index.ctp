<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author[]|\Cake\Collection\CollectionInterface $authors
 */
?>


<h3 class="display-4 pt-3"><?= __('Authors') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
<thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('author') ?></th>
            <th scope="col"><?= $this->Paginator->sort('institution') ?></th>
            <th scope="col"><?= $this->Paginator->sort('email') ?></th>
            <th scope="col"><?= $this->Paginator->sort('orcid_id') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authors as $author): ?>
        <tr align="left">
            <td><?= $this->Html->link($author->author, ['action' => 'view', $author->id]) ?></td>
            <td><?= h($author->institution) ?></td>
            <td><?= h($author->email) ?></td>
            <td><?= h($author->orcid_id) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

