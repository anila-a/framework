<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource $externalResource
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($externalResource) ?>
            <legend class="capital-heading"><?= __('Edit External Resource') ?></legend>
            <?php
                echo $this->Form->control('external_resource');
                echo $this->Form->control('base_url');
                echo $this->Form->control('project_url');
                echo $this->Form->control('abbrev');
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $externalResource->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $externalResource->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List External Resources'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
