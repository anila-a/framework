<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EntryType[]|\Cake\Collection\CollectionInterface $entryTypes
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Entry Type'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Entry Types') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('label') ?></th>
            <th scope="col"><?= $this->Paginator->sort('author') ?></th>
            <th scope="col"><?= $this->Paginator->sort('title') ?></th>
            <th scope="col"><?= $this->Paginator->sort('journal') ?></th>
            <th scope="col"><?= $this->Paginator->sort('year') ?></th>
            <th scope="col"><?= $this->Paginator->sort('volume') ?></th>
            <th scope="col"><?= $this->Paginator->sort('pages') ?></th>
            <th scope="col"><?= $this->Paginator->sort('number') ?></th>
            <th scope="col"><?= $this->Paginator->sort('month') ?></th>
            <th scope="col"><?= $this->Paginator->sort('eid') ?></th>
            <th scope="col"><?= $this->Paginator->sort('note') ?></th>
            <th scope="col"><?= $this->Paginator->sort('crossref') ?></th>
            <th scope="col"><?= $this->Paginator->sort('keyword') ?></th>
            <th scope="col"><?= $this->Paginator->sort('doi') ?></th>
            <th scope="col"><?= $this->Paginator->sort('url') ?></th>
            <th scope="col"><?= $this->Paginator->sort('file') ?></th>
            <th scope="col"><?= $this->Paginator->sort('citeseerurl') ?></th>
            <th scope="col"><?= $this->Paginator->sort('pdf') ?></th>
            <th scope="col"><?= $this->Paginator->sort('abstract') ?></th>
            <th scope="col"><?= $this->Paginator->sort('comment') ?></th>
            <th scope="col"><?= $this->Paginator->sort('owner') ?></th>
            <th scope="col"><?= $this->Paginator->sort('timestamp') ?></th>
            <th scope="col"><?= $this->Paginator->sort('review') ?></th>
            <th scope="col"><?= $this->Paginator->sort('search') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publisher') ?></th>
            <th scope="col"><?= $this->Paginator->sort('editor') ?></th>
            <th scope="col"><?= $this->Paginator->sort('series') ?></th>
            <th scope="col"><?= $this->Paginator->sort('address') ?></th>
            <th scope="col"><?= $this->Paginator->sort('edition') ?></th>
            <th scope="col"><?= $this->Paginator->sort('howpublished') ?></th>
            <th scope="col"><?= $this->Paginator->sort('lastchecked') ?></th>
            <th scope="col"><?= $this->Paginator->sort('booktitle') ?></th>
            <th scope="col"><?= $this->Paginator->sort('organization') ?></th>
            <th scope="col"><?= $this->Paginator->sort('language') ?></th>
            <th scope="col"><?= $this->Paginator->sort('chapter') ?></th>
            <th scope="col"><?= $this->Paginator->sort('type') ?></th>
            <th scope="col"><?= $this->Paginator->sort('school') ?></th>
            <th scope="col"><?= $this->Paginator->sort('nationality') ?></th>
            <th scope="col"><?= $this->Paginator->sort('yearfiled') ?></th>
            <th scope="col"><?= $this->Paginator->sort('assignee') ?></th>
            <th scope="col"><?= $this->Paginator->sort('day') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dayfiled') ?></th>
            <th scope="col"><?= $this->Paginator->sort('monthfiled') ?></th>
            <th scope="col"><?= $this->Paginator->sort('institution') ?></th>
            <th scope="col"><?= $this->Paginator->sort('revision') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($entryTypes as $entryType): ?>
        <tr>
            <!-- <td><?= $this->Number->format($entryType->id) ?></td> -->
            <td><?= h($entryType->label) ?></td>
            <td><?= h($entryType->author) ?></td>
            <td><?= h($entryType->title) ?></td>
            <td><?= h($entryType->journal) ?></td>
            <td><?= h($entryType->year) ?></td>
            <td><?= h($entryType->volume) ?></td>
            <td><?= h($entryType->pages) ?></td>
            <td><?= h($entryType->number) ?></td>
            <td><?= h($entryType->month) ?></td>
            <td><?= h($entryType->eid) ?></td>
            <td><?= h($entryType->note) ?></td>
            <td><?= h($entryType->crossref) ?></td>
            <td><?= h($entryType->keyword) ?></td>
            <td><?= h($entryType->doi) ?></td>
            <td><?= h($entryType->url) ?></td>
            <td><?= h($entryType->file) ?></td>
            <td><?= h($entryType->citeseerurl) ?></td>
            <td><?= h($entryType->pdf) ?></td>
            <td><?= h($entryType->abstract) ?></td>
            <td><?= h($entryType->comment) ?></td>
            <td><?= h($entryType->owner) ?></td>
            <td><?= h($entryType->timestamp) ?></td>
            <td><?= h($entryType->review) ?></td>
            <td><?= h($entryType->search) ?></td>
            <td><?= h($entryType->publisher) ?></td>
            <td><?= h($entryType->editor) ?></td>
            <td><?= h($entryType->series) ?></td>
            <td><?= h($entryType->address) ?></td>
            <td><?= h($entryType->edition) ?></td>
            <td><?= h($entryType->howpublished) ?></td>
            <td><?= h($entryType->lastchecked) ?></td>
            <td><?= h($entryType->booktitle) ?></td>
            <td><?= h($entryType->organization) ?></td>
            <td><?= h($entryType->language) ?></td>
            <td><?= h($entryType->chapter) ?></td>
            <td><?= h($entryType->type) ?></td>
            <td><?= h($entryType->school) ?></td>
            <td><?= h($entryType->nationality) ?></td>
            <td><?= h($entryType->yearfiled) ?></td>
            <td><?= h($entryType->assignee) ?></td>
            <td><?= h($entryType->day) ?></td>
            <td><?= h($entryType->dayfiled) ?></td>
            <td><?= h($entryType->monthfiled) ?></td>
            <td><?= h($entryType->institution) ?></td>
            <td><?= h($entryType->revision) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $entryType->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $entryType->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $entryType->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $entryType->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

