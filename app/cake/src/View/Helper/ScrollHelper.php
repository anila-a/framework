<?php
namespace App\View\Helper;

use Cake\View\Helper;

class ScrollHelper extends Helper
{
    public function toTop()
    {
        return implode("\n", ['<button class="p-0 mt-5 btn go-top-btn d-flex align-items-center justify-content-between mx-auto">', '<a href="#" class="text-dark btn px-5 py-2">Back to top', '<span class="fa fa-long-arrow-up ml-2" aria-hidden="true"></span>', '</a>', '</button>']);
    }
}
