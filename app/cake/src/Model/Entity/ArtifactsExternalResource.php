<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsExternalResource Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property int|null $external_resource_id
 * @property string|null $external_resource_key
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\ExternalResource $external_resource
 */
class ArtifactsExternalResource extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'external_resource_id' => true,
        'external_resource_key' => true,
        'artifact' => true,
        'external_resource' => true
    ];
}
