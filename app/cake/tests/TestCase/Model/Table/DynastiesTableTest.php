<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DynastiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DynastiesTable Test Case
 */
class DynastiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DynastiesTable
     */
    public $Dynasties;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dynasties',
        'app.proveniences',
        'app.dates',
        'app.rulers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Dynasties') ? [] : ['className' => DynastiesTable::class];
        $this->Dynasties = TableRegistry::getTableLocator()->get('Dynasties', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dynasties);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
