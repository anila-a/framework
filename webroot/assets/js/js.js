$(document).ready(function() {
    $('#select-table-dropdown').on('change', function() {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        var baseUrl = window.location.origin;

        var newUrl = baseUrl + '/stats/' + valueSelected;

        window.location.href = newUrl;
    });

    // Non-essential keyboard interaction for the Accordion pattern
    // https://www.w3.org/TR/wai-aria-practices-1.1/#keyboard-interaction
    $('.accordion > section > input[type="checkbox"]').keydown(function(e){
        switch(e.which) {
            // Enter: toggle accordion part
            case 13: $(this).click(); break;
            // Home & End, Up+Down arrows: move focus
            case 35: $(this).parent().siblings().last().children().first().focus(); break;
            case 36: $(this).parent().siblings().first().children().first().focus(); break;
            case 38: $(this).parent().prev().children().first().focus(); break;
            case 40: $(this).parent().next().children().first().focus(); break;
            default: return;
        }
        e.preventDefault();
    });
});
