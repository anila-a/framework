# API development guide

This is a guide for developing and maintaining APIs in the framework.

## Entity APIs

This section concerns GET endpoints.

### Request type recognition

The request type can be determined using [`RequestHandler::accepts()`](https://api.cakephp.org/3.6/class-Cake.Controller.Component.RequestHandlerComponent.html#_accepts).
For example, to test whether the client requests a JSON file one can use

```php
$this->RequestHandler->accepts('json')
```

To implement content negotiation, it may be better to use [`RequestHandler::prefers()`](https://api.cakephp.org/3.6/class-Cake.Controller.Component.RequestHandlerComponent.html#_prefers)
instead. This prioritizes the users request over the order of implementations.

```php
$preferredType = $this->RequestHandler->prefers()
if ($preferredType === 'json') {
    // ...
}
```

See the section on content types for supported values.

### Response serialization

A number of formats can be serialized out of the box. CakePHP by default has
data views for JSON and XML, and the CsvView plugin adds support for a CSV data
view, currently also used for XLSX and TSV. To serialize custom formats add a
view class to the `viewClassMap` option of `RequestHandler` in the `initialize()`
function of `AppController.php`. These custom `View` classes can extend other
classes, for example to pre-process the data before serialization.

#### APIs needing external scripts

For APIs needing to generate output by means outside of the PHP program, see the
section on [Script APIs](#script-apis) on how to use external scripts.

### Response type specification

You can specify the response type with [`RequestHandler::respondAs()`](https://api.cakephp.org/3.6/class-Cake.Controller.Component.RequestHandlerComponent.html#_respondAs)
like the code block below. This is usually called in the data view, but in the case
of CsvView the response type is overridden after the `render()` call in case
not CSV, but TSV or XLSX is requested.

```php
$this->render();
$this->RequestHandler->respondAs('json');
```

See the section on content types for supported values.

### Content types

The map of content types is relatively complete and will
not need extension for most common types. You can check whether a type is supported
in [`Response::_mimeTypes` (protected)](https://api.cakephp.org/3.6/class-Cake.Http.Response.html#$_mimeTypes). To add a custom type anyway, use
[`Response::setTypeMap()`](https://api.cakephp.org/3.6/class-Cake.Http.Response.html#_setTypeMap) in the `initialize()` function of `AppController`, generally.

```php
EventManager::instance()->on('Controller.initialize', function (Event $event) {
    $controller = $event->getSubject();
    $controller->response->setTypeMap('json', 'application/json');
});
```

## Script APIs
This section concerns POST endpoints that take in input and convert it by use of external
scripts.

TODO
